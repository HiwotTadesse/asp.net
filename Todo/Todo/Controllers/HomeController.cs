﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Todo.Models;

namespace Todo.Controllers
{
    public class HomeController : Controller
    {

        private readonly StoryDBOperation context;

        private IList<Story> Stories { get; set; }

        public HomeController(StoryDBOperation context)
        {
            this.context = context;
        }

        [HttpPost]
        [Route("/Index/Add")]
        public async Task<IActionResult> SubmitForm([FromBody]Story storyData)
        {
            Story story = new Story
            {
                content = storyData.content
            };
            context.Stories.Add(story);
            await context.SaveChangesAsync();

            return View("Index");
        }

        public async Task GetTaskAsync()
        {
            Stories = context.Stories.ToList();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("/Index/List")]
        public JsonResult ListOfStories()
        {
            var stories = context.Stories.ToList();
            return new JsonResult(stories);
        }
        [HttpPost("/Index/delete/{id}")]
        public JsonResult DeleteStory([FromRoute]int id)
        {
          Story toBeDeleted =  context.Stories.Where(x => x.ID == id).FirstOrDefault<Story>();
            context.Stories.Remove(toBeDeleted);
            context.SaveChanges();
            return new JsonResult(new { message="Successfully Deleted"});
        }

        [HttpPut("/Index/edit/{id}")]
        public async Task<IActionResult> EditStory([FromRoute]int id, [FromBody]string storyData)
        {
            Story story = new Story
            {
                content = storyData,
                ID = id
            };
            context.Stories.Update(story);
            context.SaveChanges();
            return new JsonResult(new { message = "Successfully Updated" });
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
