﻿

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.Models
{
    public class StoryDBOperation:DbContext
    {
        public StoryDBOperation(DbContextOptions<StoryDBOperation> options) : base(options)
        {

        }

        public DbSet<Story> Stories  { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {

            modelBuilder.Entity<Story>().ToTable("Story");
        }
}

    
}
